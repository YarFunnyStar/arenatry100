﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int damage;
    public int health;

    public GameObject blood;
    public GameObject deathEffect;
    public GameObject AfterDeathSpriteMask;
    //public GameObject HealthBar;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<Player>().TakeDamage(damage);
        }

        if (collision.tag == "Bullet")
        {
            TakeDamage(GameObject.Find("Gun").GetComponent<Aim>().CurrentWeapon.Damage);
        }
       
    }

    public void TakeDamage(int damage) 
    {

        health -= damage;
        //HealthBar.transform.localScale = new Vector3(health / 100, HealthBar.transform.localScale.y, HealthBar.transform.localScale.z);
        if (health <= 0)
        { 
            Destroy(gameObject);
            Instantiate(deathEffect, transform.position, Quaternion.identity);
            Instantiate(AfterDeathSpriteMask, transform.position, Quaternion.identity);
        } else {
            Instantiate(blood, transform.position, Quaternion.identity);
        }
    }

}
