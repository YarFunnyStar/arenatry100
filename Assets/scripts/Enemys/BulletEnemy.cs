﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : MonoBehaviour
{
    private float lifeTime = 5;
    public string tagDestroy;
    public string tagDestroy1;
    
    private void Awake()
    {
        Destroy(gameObject, lifeTime);
    }

    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == tagDestroy || collision.tag == tagDestroy1)
        {

            Destroy(gameObject, 0);
        }
    }
}
