﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveEnemy : Enemy
{
    public LayerMask player;
    string playerTag = "Player";

    public bool CheckHowMove;
    public float SpeedMove;
    public float RelaxDistance;


    private bool CheckPoint;
    private int CheckElement = 0;
    private List<GameObject> waypointsList;//массив точек в игре
    private int waypointsCount = 0; //счетчик точек в игре
    private GameObject Target; //текущая цель

    public float bulletForce = 10f;
    public float radius;
    public Transform PointGun;
    public GameObject bulletPrefab;
    public float dalay;
    private float time;
    void Start()
    {
      

    }
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.tag == "Player")
    //    {
           
    //        Debug.Log("lol");
    //    }
    //}

    void Update()
    {
        Collider2D collider = Physics2D.OverlapCircle(transform.position, radius, player);
        if (collider == true)
        {
            Fire_1();
        }

        if (CheckHowMove == true)
        {
            MoveOnPlayer();
        }
        else
        {
            MoveOnPoint();
        }
        Fire_1();
    }

    public void MoveOnPlayer()
    {
        GameObject player = GameObject.FindGameObjectWithTag(playerTag);
        var dir = player.transform.position - transform.position;
        if (dir.sqrMagnitude > RelaxDistance * RelaxDistance)
        {
            float step = SpeedMove * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, step);


        }
    }

    public void MoveOnPoint()
    {
        waypointsList = new List<GameObject>(GameObject.FindGameObjectsWithTag("PointMove"));
        waypointsCount = waypointsList.Count; //количество точек в массиве


        if (Target != null && CheckPoint == true) //если у нас есть цель
        {
            float step = SpeedMove * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, step);
            if (Target.transform.position - transform.position == new Vector3(0, 0, 0))
            {
                CheckPoint = false;
                CheckElement = Random.Range(0, waypointsCount);
                
            }
        }
        else //если нету цели
        {

            Target = waypointsList[CheckElement];
            CheckPoint = true;


        }


    }

    public void Fire_1()
    {
        

        if (time > 0)
        {
            time -= Time.deltaTime;
        }
        else
        {
            time = dalay;
        }
        

        if (time <= 0)
        { 
            GameObject bullet = Instantiate(bulletPrefab, PointGun.position, Quaternion.identity);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(PointGun.forward * bulletForce, ForceMode2D.Impulse);
            
        }


    }
    public void Fire_2()
    {
        if (time > 0)
        {
            time -= Time.deltaTime;
        }
        else
        {
            time = dalay;
        }


        if (time <= 0)
        {
            

            
        }
    }
    public void Fire_3()
    {

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radius);

    }

}
