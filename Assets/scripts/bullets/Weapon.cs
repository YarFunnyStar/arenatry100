﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Weapon")]
public class Weapon : ScriptableObject
{
    public Sprite CurrentWeaponSprite;
    public GameObject BulletPrefab;
    public float FireRate = 1;
    public int Damage = 1;
    
    
    public void Shoot()
    {
        GameObject bullet = Instantiate(BulletPrefab, GameObject.Find("FirePoint").transform.position, Quaternion.identity);
    }



    
}
