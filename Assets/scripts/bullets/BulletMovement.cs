﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public float speed;
    private Vector2 dir;
    public float lifeTime;
    public GameObject SpawnEffect;
    public GameObject HitEffect;

    private void Awake()
    {
        Destroy(gameObject, lifeTime);
    }
    void Start()
    {
        
        dir = GameObject.Find("Dir").transform.position;
        transform.position = GameObject.Find("FirePoint").transform.position;
        transform.eulerAngles = new Vector3(0, 0, GameObject.Find("Gun").transform.rotation.eulerAngles.z);
        Instantiate(SpawnEffect, transform.position, Quaternion.identity);
      

    }

    // Update is called once per frame
    void Update()
    {
        
        transform.position = Vector2.MoveTowards(transform.position, dir, speed * Time.deltaTime);
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy" || collision.tag == "Wall")
        {

            Destroy(gameObject, 0);
            Instantiate(HitEffect, transform.position, Quaternion.identity);
        }
        
    }
   

}
