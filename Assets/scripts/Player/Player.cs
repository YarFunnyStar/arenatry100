﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private float MoveInputHorizontal;
    private float MoveInputVertical;
    public float speed;

    public int health;
    public GameObject DeathEffect;
    public GameObject DamageEffect;

    private Rigidbody2D rb;
    public Animator anim;

    

    




    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        Movement();
       
    }





    public void TakeDamage(int Damage)
    {
        FindObjectOfType<CameraShake>().Shake();
        if (health > 1)
        {
            Instantiate(DamageEffect, transform.position, Quaternion.identity);
        }
        health -= Damage;
        print(health);
        if (health <= 0)
        {
            Destroy(gameObject);
            Instantiate(DeathEffect, transform.position, Quaternion.identity);
        }
    }

    public void Movement()
    {
        //MOVEMENT
        MoveInputHorizontal = Input.GetAxisRaw("Horizontal");
        MoveInputVertical = Input.GetAxisRaw("Vertical");
        rb.velocity = new Vector2(MoveInputHorizontal * speed, MoveInputVertical * speed);
        //MOVEMENT



        //ANIMATIONS
        anim.SetFloat("Horizontal", MoveInputHorizontal);
        anim.SetFloat("Vertical", MoveInputVertical);


        if (MoveInputHorizontal != 0 || MoveInputVertical != 0)
        {
            anim.SetBool("IsMoving", true);
        }
        else { anim.SetBool("IsMoving", false); }
        //ANIMATIONS
    }


    public void AddHealth(addhealth Serdco)
    {
        health = Serdco.Health + health;
        Destroy(Serdco.gameObject);
    }




}























