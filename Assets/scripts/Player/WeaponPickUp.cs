﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickUp : MonoBehaviour
{
    public Weapon ThisWeapon;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "GunPicker")
        {
            collision.GetComponent<Aim>().CurrentWeapon = ThisWeapon;
            collision.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = ThisWeapon.CurrentWeaponSprite;
            Destroy(gameObject);
        }
    }
}
