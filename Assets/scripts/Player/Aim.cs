﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour
{
  
    private float NextTimeOffFire;

    public Weapon CurrentWeapon;

    void Start()
    {
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = CurrentWeapon.CurrentWeaponSprite;
    }
    
    void Update()
    {

        if(Input.GetMouseButton(0))
        {
            if (Time.time >= NextTimeOffFire)
            {
                CurrentWeapon.Shoot();
                NextTimeOffFire = Time.time + 1 / CurrentWeapon.FireRate;
            }
        }





        WeaponRotation();


       
    }
    void WeaponRotation() // vot eto vot povorachivaet nashego persa sled za mishkoi, kak eto rabotaet - naychnaya fantastika
    {
        Vector2 dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 10 * Time.deltaTime);
    }

    
}
