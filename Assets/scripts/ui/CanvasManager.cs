﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour
{

    public GameObject PanelFadeIn, PanelFadeOut, pauseUI;

    public void Start()
    {
       
        pauseUI.SetActive(false);
        PanelFadeIn.SetActive(false);
        StartCoroutine(FadeOut());

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !pauseUI.activeSelf)
        {
            Time.timeScale = 0;
            pauseUI.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && pauseUI.activeSelf)
        {
            Time.timeScale = 1;
            pauseUI.SetActive(false);
        }
    }

    public void LoadScene(string sceneName)
    {
        pauseUI.SetActive(false);
        StartCoroutine(FadeInSceneLoad(sceneName));
    }
    IEnumerator FadeInSceneLoad(string sceneName)
    {
        Time.timeScale = 1;
        PanelFadeIn.SetActive(true);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(sceneName);

    }
    IEnumerator FadeOut()
    {
        PanelFadeOut.SetActive(true);
        Time.timeScale = 1;
        yield return new WaitForSeconds(1f);
        PanelFadeOut.SetActive(false);
    }

    
    IEnumerator RestartLvl()
    {
        PanelFadeIn.SetActive(true);
        Time.timeScale = 1;
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
   
    public void Continue()
    {
        pauseUI.SetActive(false);
        Time.timeScale = 1;
    }
    public void Restart()
    {
       
        pauseUI.SetActive(false);
        PanelFadeIn.SetActive(true);
        StartCoroutine(RestartLvl());
    }
    


}
