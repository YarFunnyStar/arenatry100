﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Shop : MonoBehaviour
{
    public TextMeshProUGUI moneyText;
    public int money;
    public TextMeshProUGUI itemAmountText;
    public int itemAmount;
    public TextMeshProUGUI itemPriceText;
    public int itemPrice;
    public TextMeshProUGUI weaponBought;
    public bool weapon = false;
    public Button buyWeaponButton;


    public void Start()
    {
        money = int.Parse(moneyText.text);
        //moneyText.text = money.ToString();
        itemPrice = int.Parse(itemPriceText.text);
    }
    public void buyItem()
    {
        if (money - itemPrice>=0)
        {
            money = int.Parse(moneyText.text);
            money -= itemPrice;
            moneyText.text = money.ToString();
            itemAmount++;
            itemAmountText.text = "X " + itemAmount.ToString();
        }
    }
    public void buyWeapon()
    {
        if (money - itemPrice >= 0&&!weapon)
        {
            money = int.Parse(moneyText.text);
            money -= itemPrice;
            moneyText.text = money.ToString();
            weapon = true;
            weaponBought.text = "BOUGHT";
            buyWeaponButton.interactable = false;
        }
    }
}
