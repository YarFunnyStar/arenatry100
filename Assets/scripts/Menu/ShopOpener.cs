﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopOpener : MonoBehaviour
{
    public GameObject shopMenu;
    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player"&& Input.GetKey(KeyCode.E))
        {
            shopMenu.SetActive(true);
        }
    }
    private void Update()
    {
        if (shopMenu.activeSelf && Input.GetKeyDown(KeyCode.Escape))
            shopMenu.SetActive(false);
    }
}
