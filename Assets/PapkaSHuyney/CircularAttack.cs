﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularAttack : MonoBehaviour
{
    public float bulletForce;
    public int damage;

    void Start()
    {

    }


    void Update()
    {


        transform.Translate(Vector3.right * bulletForce * Time.deltaTime);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<Player>().TakeDamage(damage);
        }


        if (collision.tag == "Player" || collision.tag == "wall")
        {

            Destroy(gameObject, 0);
        }
    }
}
